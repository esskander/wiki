###Создаём файловую систему на разделе (при желании можно задать лэйбл ключом -L):

```mkfs.btrfs -f /dev/sda1```

###Теперь монтируем:

```mount /dev/sda1 /mnt```

###Затем создадим три подтома под корень, домашние каталоги пользователей и для хранения снимков:

```btrfs subvolume create /mnt/sv_root```

```btrfs subvolume create /mnt/sv_home```

```btrfs subvolume create /mnt/sv_snapshots```


###И отмонтируем корень ФС:


```umount /mnt```

###Далее, для того, чтобы монтировать подтом подобно обычному разделу диска, команде mount нужно указывать опцию subvol=PATH, где PATH -L): путь относительно корня ФС. Монтируем корень:

```mount -o subvol=sv_root,compress=lzo,autodefrag /dev/sda1 /mnt```

###Создаём папку и монтируем в неё наш будущий каталог пользователей:

```mkdir /mnt/home```

```mount -o subvol=sv_home,compress=lzo,autodefrag /dev/sda1 /mnt/home```

###Создаём папку, где будут храниться снимки:

```mkdir /mnt/snapshots```

```mount -o subvol=sv_snapshots,compress=lzo,autodefrag /dev/sda1 /mnt/snapshots```

###Создаем снимок.

###Для этого сперва монтируем раздел btrfs

```sudo mount /dev/sdb2 /mnt/btrfs```

###Делаем снапшот

```sudo btrfs subvolume snapshot /mnt/btrfs/sv_root/ /mnt/btrfs/sv_snapshots/root_24-05-2018```

###Размонтируем

```sudo umount /mnt```
