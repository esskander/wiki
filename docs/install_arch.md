###Установка Arch Linux на зашифрованный раздел


Создание загрузочной флэшки:

```sudo mkfs.vfat /dev/sdX -I```

Запись образа dd

```sudo dd bs=4M if=ctlos.iso of=/dev/sdX status=progress && sync```

После загрузки с флэшки можно подредактировать файлик /etc/pacman.d/mirrorlist передвинув вверх сервера своей страны

Посмотрим что есть за диски

```lsblk```
```gdisk /dev/sda```

Жмем "o" и создаем новую таблицу

Жмем "n" и создаем новый раздел

Пару раз нажмем Enter выбрав значения по умолчанию и установим размер "+512MB"

Далее он спрашивает нас тип файловой системы на этом разделе, нажмем "L" и введем "EFI"

Он нам говорит что код для нашей файловой системы ef00

Далее создаем второй раздел нажав "n" и далее enter 4 раза приняв значения по умолчанию

После создания двух разделов записываем изменения нажав клавишу "w" и подтверждаем

Проверим что разделы создались:

```lsblk```

Теперь мы можем зашифровать наш раздел на котором будет linux:

```cryptsetup -y luksFormat /dev/sda2```	

Вводим "YES" большими буквами

Вводим пароль

Теперь мы можем его открыть:

```cryptsetup open /dev/sda2 cryptroot```   где cryptroot просто название директории которая будет в /dev/mapper

Создаем на зашифрованном разделе файловую систему:

```mkfs.ext4 /dev/mapper/cryptroot```

Теперь можем монтировать ФС в mnt:

```mount /dev/mapper/cryptroot /mnt```

Перейдем в mnt и создадим там папку boot

```cd /mnt```
```mkdir boot```

Сделаем файловую систему на первом разделе EFI и смонтируем в boot:

```mkfs.vfat /dev/sda1```
```mount /dev/sda1 /mnt/boot```

# Теперь начинаем установку arch в /mnt

```pacstrap /mnt base base-devel```

Переходим в установленную систему:

```arch-chroot /mnt``` 

Установим в boot все необходимые для загрузки файлы:

```bootctl install```

Установим vim

```pacman -S vim sudo gcc make ```

Теперь добавим в дро поддержку шифрования. Для этого в файле ```/etc/mkinitcpio.conf``` необходимо найти строку ```HOOKS...```  и в скобки к прочим параметрам добавить ```encrypt```

Переходим в /boot/loader/ и редактируем файл loader.conf

```cd /boot/loader/```
```vim loader.conf```

Там коментируем строку default и добавим:

```timeout 3```
```default arch```

Переходим в entries:

```cd entries```

И добавляем файл arch.conf с содержимым где вводим UUID нашего зашифрованного раздела

```title Arch```

```linux /vmlinuz-linux```

```initrd /initramfs-linux.img```

```options rw cryptdevice=UUID=ТУТUUUIDРАЗДЕЛА:cryptroot root=/dev/mapper/cryptroot```

Теперь нужно пересобрать ядро:

```mkinitcpio -p linux```

Меняем пароль root

```passwd```

Добавляем пользователя

```useradd -m -g users -G wheel -s /bin/bash MYUSERNAME```

После чего  SUDO для этого откроем файл /etc/sudoers и после строчки muser ALL=(ALL) ALL добавим такую же только вместо root впишем имя только что созданного пользоателя


Перезагружаемся, входим под root, если интернет проводной то вводим:

```dhcpcd```

Если интернет wifi:

```wifi-menu```

Добавим русскую локаль в систему

```echo -e "en_US.UTF-8 UTF-8\nru_RU.UTF-8 UTF-8" >> /etc/locale.gen```

Настроим PACMAN 

```vim /etc/pacman.conf```

Для работы 32-битных приложений в 64-битной системе необходимо раскомментировать репозиторий multilib:

```[multilib]
Include = /etc/pacman.d/mirrorlist```

после чего обновляем PACMAN:

```pacman -Syy```

Теперь можно поставить иксы и драйвера

```pacman -S xorg-server xorg-drivers```

Ставим Xfce + менеджер входа lxdm (или sddm)

```pacman -S xfce4 xfce4-goodies lxdm```

Ставим шрифты, чтобы можно было читать, что написано. Иначе будут просто квадратики.

```pacman -S ttf-liberation ttf-dejavu```

Ставим менеджер сети

```pacman -S networkmanager network-manager-applet ppp```

Подключаем автозагрузку менеджера входа и интернет (с соблюдением регистра для NetworkManager)

```systemctl enable lxdm NetworkManager```

Перезагружаемся, выбираем Xfce Session или просто “Сеанс”!!!!! Это важно иначе не войдете!


#Установка AUR

Обновляем систему

```sudo pacman -Syu```
```pacman -S binutils```

Создаём yay_install директорию и переходим в неё

```mkdir -p /tmp/yay_install```
```cd /tmp/yay_install```

Установка "yay" из AUR

```sudo pacman -S git```
```git clone https://aur.archlinux.org/yay.git```
```cd yay```
```makepkg -sir --needed --noconfirm --skippgpcheck```
```rm -rf yay_install```

Если вы предпочитаете работать с графическим интерфейсом, а не с терминалом то как альтернативу yay можно использовать pamac (установщик пакетов из AUR c GUI)

Обновляем систему

```sudo pacman -Syu```

Создаём pamac-aur_install директорию и переходим в неё

```mkdir -p /tmp/pamac-aur_install```
```cd /tmp/pamac-aur_install```


```Установка "pamac-aur" из AUR

```sudo pacman -S git```
```git clone https://aur.archlinux.org/pamac-aur.git```
```cd pamac-aur```
```makepkg -si --needed --noconfirm --skippgpcheck```
```rm -rf pamac-aur_install```

