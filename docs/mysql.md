### MYSQL

##Создать пользователя

```CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';```


##Создание базы:

Входим в mysql

```mysql -u root -p```

Далее команды:

```CREATE DATABASE 'db';
mysql> GRANT ALL PRIVILEGES ON 'db'.* TO 'name'@'localhost';```

Обновим привелегии:

`FLUSH PRIVILEGES;`

Выйдем:

`exit`

##Загрузка дампа:


```mysql --user=user --password=password DB < file_name.sql```

##Создание дампа и сохранение его в файл имя которого будет текущая дата :

```mysqldump -uroot -p crm > 'date "+%Y-%m-%d"'.sql```

