#Установка gitbook

```sudo apt install curl vim```

```sudo apt-get install -y nodejs```

```sudo apt-get install gcc g++ make```

```curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -```

``` echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list```

```sudo apt-get update && sudo apt-get install yarn```

```curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -```
```sudo apt-get install -y nodejs```

```node -v && npm -v```

```sudo npm install -g npm@latest```

```sudo mkdir /var/www/gitbook```

```cd /var/www/gitbook```

```npm install gitbook-cli -g```

```gitbook init```

Создаем файл ```vim book.json``` пишем туда:

```
{
"root": "./knowledge_base",
"title": "dpkb",
"author": "alex",
"description": "just my knowledge",
"plugins": [
            "folding-chapters-2",
            "theme-code",
            "-sharing"
           ],
 "pluginsConfig": {
         "theme-default": {
                 "showLevel": false
  }
 }
}
```
```gitbook install```

```mkdir knowledge_base```

```cp README.md knowledge_base/```

```cp SUMMARY.md knowledge_base/```

в файле SUMMARY должны быть перечислены все файлы md

Запуск сервера ```sudo gitbook serve```
