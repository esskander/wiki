###LUKS + BTRFS + UEFI


Создаем зашифрованный раздел

```cryptsetup luksFormat /dev/sda2```

Пишем  YES большими буквами.

Открываем вновь созданный зашифрованный раздел:

```cryptsetup open /dev/sda2 btrfs-system```

Форматируем разделы:

```mkfs.vfat -F32 /dev/sda1```

```mkfs.btrfs -L btrfs /dev/mapper/btrfs-system```

Создаем btrfs разделы:

```mount /dev/mapper/btrfs-system /mnt```

```btrfs subvolume create /mnt/root```

```btrfs subvolume create /mnt/home```

```umount /mnt```

```mount -o subvol=root,ssd,compress=lzo,discard /dev/mapper/btrfs-system /mnt```

```mkdir /mnt/{boot,home,swap}```

```mount -o subvol=home,ssd,compress=lzo,discard /dev/mapper/btrfs-system /mnt/home```

Монтируем загрузочный раздел:

```mount /dev/sda1 /mnt/boot```

Инсталируем:

```pacstrap /mnt base base-devel btrfs-progs vim efibootmgr sudo zsh zsh-completions grml-zsh-config intel-ucode```

Генерируем fstab:

```genfstab -U /mnt > /mnt/etc/fstab```

Переходим в установленную систему и меняем шел:

```arch-chroot /mnt /bin/zsh```

Задаем имя машины:

```echo computer_name > /etc/hostname```

Меняем шел по умолчанию:

```chsh -s /bin/zsh``

Выставляем часовой пояс:

```ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime```

Зададим кодировки, раскоментируем ru_RU.UTF-8 and en_US.UTF-8

```vim /etc/locale.gen```

```locale-gen```

```echo LANG=en_GB.UTF-8 > /etc/locale.conf```

```echo KEYMAP=sv-latin1 > /etc/vconsole.conf```

Меняем пароль суперпользователя:

```passwd```

Правим настройки ядра:

```vim /etc/mkinitcpio.conf``` Пишем туда:

```HOOKS="base udev autodetect modconf block keyboard encrypt keymap filesystems" ```

Генерируем ядро:

```mkinitcpio -p linux```

Установим все что нужно в boot:

```bootctl install```

Посмотрим UUID дисков:

```lsblk -o name,uuid``` 

Получим примерно такой вывод:


```NAME UUID```

sda

```├─sda1 AB0E-6CD2```

```└─sda2 b6cde26f-a4de-4f2e-b973-57c8fbe8813b```

```└─btrfs-system 5f55843f-d4ef-4c9c-bb43-d6ceb0d99c4d```

Создадим точку входа ```/boot/loader/entries/arch.conf``` с таким содержанием:

```title Arch Linux```

```linux /vmlinuz-linux```

```initrd /intel-ucode.img```

```initrd /initramfs-linux.img```

```options rd.luks.name=b6cde26f-a4de-4f2e-b973-57c8fbe8813b=btrfs-system luks.options=discard root=/dev/mapper/btrfs-system rw rootflags=subvol=root quiet```

Чуть подправим менюшку ```/boot/loader/loader.conf``` внесем туда:

```timeout 3```

```default arch```

ВСЕ ДОЛЖНО РАБОТАТЬ!!!
